# Make variables for the gnu compiler on lxwrk
#============================================================

# Fortran
FC = gfortran
FFLAGS_OPT = -O2
FFLAGS_DEBUG = -O0 -g -traceback
FFLAGS_FLOAT_CONTROL =
FFLAGS_64BIT_REAL = -fdefault-real-8
FFLAGS_64BIT_INT = -fdefault-integer-8
FFLAGS_MOD = -J
FFLAGS_CPP = -cpp
FFLAGS_OMP = -fopenmp
FFLAGS_NETCDF =
FFLAGS_MPI =
FFLAGS_STATIC =
FFLAGS_PIC = -fPIC

# Unique/Implementation/Component Flags
#   - Note: these are meant to allow for one off/easy additions of flags
#           for specific components. If it decided that we want to apply
#           them consistently, we make more evocative FFLAGS_* vars (as above)
FFLAGS_CANDIAG = -fconvert=big-endian -fno-range-check
# C
CC = gcc
### add C flags as necessary
CFLAGS =

# Include Flags
INCLUDEFLAGS_USR = -I/usr/include -I/home/acrnrlm/u2_test/libraries/installed/include

# Linking Flags
LDFLAGS_USR = -L/home/acrnrlm/u2_test/libraries/installed/lib
LDFLAGS_NETCDF = -L/home/acrnrlm/u2_test/libraries/installed/lib -lnetcdf -lnetcdff
LDFLAGS_CANDIAG_PLT = -L/home/acrnrlm/u2_test/ncarg/lib -lncarg -lncarg_gks -lncarg_c -L/usr/lib/X11 -lX11 -lXext -lXau -lXdmcp -L/usr/lib/x86_64-linux-gnu -lcairo -lfreetype
