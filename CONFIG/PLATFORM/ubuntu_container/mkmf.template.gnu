# GNU compiler chain on

FC := mpifort
LD := mpifort

# Set potential optimization levels
## OPT:   The fastest possible
## DEBUG: Enable debug-compatible flags
## REPRO: Ensures reproducibility while retaining some speed
OPT   ?= 0
DEBUG ?= 0
REPRO ?= 0

# By default, choose the fastest settings to compile
ifeq ($(OPT),0)
ifeq ($(DEBUG),0)
ifeq ($(REPRO),0)
OPT = 1
endif
endif
endif

# Set additional compilation options
# COMPILE_32BIT:        1 => compile with 64bit          (default off)
# COMPILE_WITH_ESMF:    1 => compile with ESMF libraries (default off)
# COMPILE_WITH_OPENMP:  1 => compile with openmp support (default on)
COMPILE_32BIT       ?= 0
COMPILE_WITH_ESMF   ?= 0
COMPILE_WITH_OPENMP ?= 1

# FFLAGS
#-------
FFLAGS_DEFAULT = -fbacktrace -cpp -fPIC -fconvert=big-endian -D_IMPI_=4 -static-libgfortran -mcmodel=medium
FFLAGS_DEFAULT += -fautomatic -pthread -w -std=legacy -finit-real=zero 
FFLAGS = $(FFLAGS_DEFAULT) 
FFLAGS_64BIT = -fdefault-real-8 -fdefault-integer-8
FFLAGS_OPENMP = -fopenmp

# Openmp support
ifeq ($(COMPILE_WITH_OPENMP),1)
FFLAGS += $(FFLAGS_OPENMP)
endif

# Set the options for various levels of optimizations
#   - Note: For now we just set the REPO flags set to the OPT flags, but this needs to be tested for reproducibility
FFLAGS_OPT = -O3
FFLAGS_REPRO = $(FFLAGS_OPT)
FFLAGS_DEBUG = -O0 -g3

# Add the optimization flags
ifeq ($(OPT),1)
FFLAGS += $(FFLAGS_OPT)
endif
ifeq ($(REPRO),1)
FFLAGS += $(FFLAGS_REPRO)
endif
ifeq ($(DEBUG),1)
FFLAGS += $(FFLAGS_DEBUG)
endif

# Add 64-bit flags if requested
ifeq ($(COMPILE_32BIT),0)
FFLAGS += $(FFLAGS_64BIT)
endif

# INCLUDE FLAGS
#---------------
INCLUDE_FLAGS_DEFAULT = -I./ -I/usr/include -I/usr/local/include -I/usr/local/mod
INCLUDE_FLAGS_ESMF =
INCLUDE_FLAGS = $(INCLUDE_FLAGS_DEFAULT)
ifeq ($(COMPILE_WITH_ESMF),1)
INCLUDE_FLAGS += $(INCLUDE_FLAGS_ESMF)
endif

# append to FFLAGS
FFLAGS += $(INCLUDE_FLAGS)

# Build the LDFLAGS varibles
LDFLAGS_OPENMP = -fopenmp 
LDFLAGS_ESMF = -lesmf
LDFLAGS_NETCDF = -lnetcdf  -lnetcdff -Wl,-Bsymbolic-functions -Wl,-z,relro -Wl,-z,now -lnetcdf -flinker-output=exec # from nconfig
LDFLAGS_DEFAULT =  -L/usr/local/lib -lmpich -lblas -llapack -static-libgfortran $(LDFLAGS_OPENMP) $(LDFLAGS_NETCDF)
LDFLAGS = $(LDFLAGS_DEFAULT) 
ifeq ($(COMPILE_WITH_ESMF),1)
LDFLAGS += $(LDFLAGS_ESMF)
endif
