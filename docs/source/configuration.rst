Model configuration
===================

.. contents:: :local:

This section gives an overview of high level configuration relevant
to most users. Low level configuration is discussed in the
Advanced section.

Choosing an experiment and model configuration
----------------------------------------------

Specifying simulation length
----------------------------

Selecting output options
------------------------

Customising input and forcing files
-----------------------------------

Selecting alternative model physics
-----------------------------------




	     
